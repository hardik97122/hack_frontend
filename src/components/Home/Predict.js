import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import * as tf from "@tensorflow/tfjs";
import { argMax, browser } from "@tensorflow/tfjs";

import { youtubeQuery } from "../../queries/queris";
import Header from "../common/Header";

const classNames = [
  "Apple Scab",
  "Apple Black Rot",
  "Apple Cedar And Rust",
  "Healthy Apple",
  "Healthy Blueberry",
  "Cherry(including_sour) Powdery Mildew",
  "Cherry (including_sour) Healthy",
  "Corn(maize Cercospora Leaf Spot Gray Leaf Spot",
  "Corn(maize) Common Rust",
  "Corn(maize) Northern Leaf Blight",
  "Corn(maize) healthy",
  "Grape Black Rot",
  "Grape Esca(Black Measles)",
  "Grape Leaf Blight(Isariopsis Leaf Spot)",
  "Grape Healthy",
  "Orange Haunglongbing(Citrus Greening)",
  "Peach Bacterial Spot",
  "Peach Healthy",
  "Pepper, Bell Bacterial Spot",
  "Pepper, Bell Healthy",
  "Potato Early Blight",
  "Potato Late Blight",
  "Potato Healthy",
  "Raspberry Healthy",
  "Soybean Healthy",
  "Squash Powdery Mildew",
  "Strawberry Leaf Scorch",
  "Strawberry Healthy",
  "Tomato Bacterial Spot",
  "Tomato Early Blight",
  "Tomato Late Blight",
  "Tomato Leaf Mold",
  "Tomato Septoria Leaf Spot",
  "Tomato Spider Mites Two Spotted Spider Mite",
  "Tomato Target Spot",
  "Tomato Yellow Leaf Curl Virus",
  "Tomato Mosaic Virus",
  "Tomato Healthy",
  "Bakcground"
];

class Predict extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: "",
      preview: "",
      image: [],
      link: [],
      text: [],
      model: null,
      name: ""
    };
  }

  componentDidMount() {
    const that = this;
    async function load() {
      const m = await tf.loadGraphModel("http://localhost:5000/model.json");
      that.setState({ ...that.state, model: m });
      console.log("loaded");
    }
    load();
  }

  fileSelectHandler = async function(event) {
    const that = this;
    if (event.target.files[0].type.includes("image")) {
      that.setState({
        ...that.state,
        selectedFile: event.target.files[0]
      });
      let reader = new FileReader();
      reader.onload = e => {
        that.setState({
          ...that.state,
          preview: e.target.result
        });
        tf.tidy(() => {
          //convert to a tensor
          let img1 = new Image();
          img1.src = e.target.result;
          img1.onload = async function() {
            let tensor = tf.image.resizeBilinear(browser.fromPixels(img1), [
              224,
              224
            ]);
            tensor = tf.expandDims(tensor, 0);
            const divideBy = tf.tensor1d([255]);
            tensor = tf.div(tensor, divideBy);
            const prediction = that.state.model.predict(tensor);
            //@ts-ignore
            prediction
              //@ts-ignore
              .array()
              .then(array => {
                const x = tf.tensor1d(array[0]);
                argMax(x)
                  .array()
                  .then(index => {
                    const diseaseName = classNames[index];
                    that.setState({ ...that.state, name: diseaseName });
                    that.loadYoutube(diseaseName);
                  });
              });
          };
        });
      };
      reader.readAsDataURL(event.target.files[0]);
      // if (event.target) event.target.value = "";
    } else {
      event.target.value = "";
      console.log("File format not supported");
    }
  };

  loadYoutube = query => {
    if (query.toLowerCase().includes("healthy")) {
      return;
    }
    var queryString = query.replace(" ", "+");
    queryString = "how+to+cure+" + queryString;
    this.props
      .youtubeQuery({
        variables: {
          query: queryString
          // query: "how+to+cure+apple+scab"
        }
      })
      .then(data => {
        console.log(data);
        this.setState({
          ...this.state,
          link: data.data.youtubesearch[0].link,
          image: data.data.youtubesearch[0].image,
          text: data.data.youtubesearch[0].text
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <div>
        <Header />
        <div className="info">
          <form>
            <div className="form-group">
              <label htmlFor="image">Upload the image</label>
              <input
                type="file"
                className="form-control-file"
                name="image"
                id="image"
                onChange={this.fileSelectHandler.bind(this)}
              />
            </div>
          </form>
          {this.state.preview ? (
            <div id="target">
              <img
                src={this.state.preview}
                className="original img-responsive img-thumbnail"
                alt="profile"
              />
            </div>
          ) : (
            ""
          )}
          {this.state.name && this.state.name.includes("Healthy") && (
            <h2 className="healthy">{this.state.name}</h2>
          )}
          {this.state.name && !this.state.name.includes("Healthy") && (
            <>
              <h2 className="disease">
                Your plant is suffering from {this.state.name}
              </h2>
              <h2>But never bother and cure it</h2>
              {this.state.image.map((im, i) => (
                <div key={i} className="vidCont">
                  <img src={im} alt={im} className="im" />
                  <a href={this.state.link[i]} className="vidlink">
                    <p>{this.state.text[i]}</p>
                  </a>
                  <br />
                  <br />
                  <hr />
                </div>
              ))}
            </>
          )}
        </div>
      </div>
    );
  }
}

export default compose(graphql(youtubeQuery, { name: "youtubeQuery" }))(
  Predict
);
