FROM node
RUN node -v

WORKDIR /frontend

COPY ./package.json .

RUN yarn -v
RUN yarn install

COPY ./ ./
ENV NODE_ENV production

EXPOSE 3000
CMD yarn start